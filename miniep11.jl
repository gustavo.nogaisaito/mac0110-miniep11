using Test
function teste()
    @test palindromo("")
    @test palindromo("ovo")
    @test !palindromo("MiniEP11")
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!")
    @test palindromo("A mãe te ama.")
    @test !palindromo("Passei em MAC0110!")
    println("Fim dos teste")
end

function palindromo(string)
    if isempty(string)
        return true
    end
    str = ""
    for i in string
        if !isletter(i)
            continue
        elseif i == 'ã' # Mesmo procurando muito não
            i = 'a'     # achei um modo de "retirar"
        elseif i == 'ô' # a acentuação das letras.
            i = 'o'
        end
        str *= i
    end
    if lowercase(str) == lowercase(reverse(str))
        return true
    end
    return false
end 

teste()

